<?php

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'mod_poasassignment';
$plugin->version   = 2021061400;  // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2018120309;  // Requires this Moodle version
$plugin->cron      = 60;          // Period for cron to check this module (secs)
$plugin->maturity  = MATURITY_RC;
$plugin->release   = '3.6 (2020.01)';
