<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * This file keeps track of upgrades to the poasassignment module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installtion to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in
 * lib/ddllib.php
 *
 * @package   mod_poasassignment
 * @copyright 2010 Your Name
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * xmldb_poasassignment_upgrade
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_poasassignment_upgrade($oldversion=0) {

    global $CFG, $THEME, $db, $DB;
    $dbman = $DB->get_manager();
    $result = true;



/// And upgrade begins here. For each one, you'll need one
/// block of code similar to the next one. Please, delete
/// this comment lines once this file start handling proper
/// upgrade code.

/// if ($result && $oldversion < YYYYMMDD00) { //New version in version.php
///     $result = result of "/lib/ddllib.php" function calls
/// }

/// Lines below (this included)  MUST BE DELETED once you get the first version
/// of your module ready to be installed. They are here only
/// for demonstrative purposes and to show how the poasassignment
/// iself has been upgraded.

/// For each upgrade block, the file poasassignment/version.php
/// needs to be updated . Such change allows Moodle to know
/// that this file has to be processed.

/// To know more about how to write correct DB upgrade scripts it's
/// highly recommended to read information available at:
///   http://docs.moodle.org/en/Development:XMLDB_Documentation
/// and to play with the XMLDB Editor (in the admin menu) and its
/// PHP generation posibilities.

/// First example, some fields were added to the module on 20070400
    if ($result && $oldversion < 2007040100) {

    /// Define field course to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $field = new XMLDBField('course');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'id');
    /// Launch add field course
        $result = $result && add_field($table, $field);

    /// Define field intro to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $field = new XMLDBField('intro');
        $field->setAttributes(XMLDB_TYPE_TEXT, 'medium', null, null, null, null, null, null, 'name');
    /// Launch add field intro
        $result = $result && add_field($table, $field);

    /// Define field introformat to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $field = new XMLDBField('introformat');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '4', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'intro');
    /// Launch add field introformat
        $result = $result && add_field($table, $field);
    }

/// Second example, some hours later, the same day 20070401
/// two more fields and one index were added (note the increment
/// "01" in the last two digits of the version
    if ($result && $oldversion < 2007040101) {

    /// Define field timecreated to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $field = new XMLDBField('timecreated');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'introformat');
    /// Launch add field timecreated
        $result = $result && add_field($table, $field);

    /// Define field timemodified to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $field = new XMLDBField('timemodified');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'timecreated');
    /// Launch add field timemodified
        $result = $result && add_field($table, $field);

    /// Define index course (not unique) to be added to poasassignment
        $table = new XMLDBTable('poasassignment');
        $index = new XMLDBIndex('course');
        $index->setAttributes(XMLDB_INDEX_NOTUNIQUE, array('course'));
    /// Launch add index course
        $result = $result && add_index($table, $index);
    }

/// Third example, the next day, 20070402 (with the trailing 00), some inserts were performed, related with the module
    if ($result && $oldversion < 2007040200) {
    /// Add some actions to get them properly displayed in the logs
        $rec = new stdClass;
        $rec->module = 'poasassignment';
        $rec->action = 'add';
        $rec->mtable = 'poasassignment';
        $rec->filed  = 'name';
    /// Insert the add action in log_display
        $result = insert_record('log_display', $rec);
    /// Now the update action
        $rec->action = 'update';
        $result = insert_record('log_display', $rec);
    /// Now the view action
        $rec->action = 'view';
        $result = insert_record('log_display', $rec);
    }
    
    if ($result && $oldversion < 2014011300) {
        // Define field grade to be added to poasassignment.
        $table = new xmldb_table('poasassignment');
        $field = new xmldb_field('grade', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '100', 'penalty');

        // Conditionally launch add field grade.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Set grade to 100 for all poasassignments.
        $DB->execute('UPDATE {poasassignment} SET grade = 100');
        //$DB->update_record_raw('poasassignment', array('grade' => 100));
        
        // Find poasassignments with non-default criterions.
        $pas = $DB->get_records('poasassignment');
        $crits = $DB->get_records('poasassignment_criterions');
        
        foreach ($pas as $pa) {
            // Collect criterions.
            $pacrits = array();
            foreach ($crits as $c) {
                if ($c->poasassignmentid == $pa->id) {
                    $pacrits[] = $c;
                }
            }
            
            // Is first criterion default?
            $firstdef = ($pacrits[0]->name == 'Grade' && $pacrits[0]->description == 'Total grade for submission');
            
            // If there is more than one criterion or the only criterion is not default, create marking guide for this PA.
            if (count($pacrits) > 1 || !$firstdef) {
                $cm = get_coursemodule_from_instance('poasassignment', $pa->id);
                $context = context_module::instance($cm->id);
                
                // Create area.
                $area = new stdClass();
                $area->contextid    = $context->id;
                $area->component    = 'mod_poasassignment';
                $area->areaname     = 'submissions';
                $area->activemethod = 'guide';
                $areaid = $DB->insert_record('grading_areas', $area);
                
                // Create definition.
                $guidedef = new stdClass();
                $guidedef->areaid            = $areaid;
                $guidedef->method            = 'guide';
                $guidedef->name              = 'Default';
                $guidedef->description       = '';
                $guidedef->descriptionformat = 1;
                $guidedef->status            = 20;
                $guidedef->timecreated       = time();
                $guidedef->usercreated       = 0;
                $guidedef->timemodified      = time();
                $guidedef->usermodified      = 0;
                $guidedef->timecopied        = 0;
                $guidedef->options           = '{"alwaysshowdefinition":"1","showmarkspercriterionstudents":"1"}';
                $defid = $DB->insert_record('grading_definitions', $guidedef);
                
                // Count summary weight.
                $sumweight = 0;
                foreach ($pacrits as $c) {
                    $sumweight += $c->weight;
                }
                
                // Create criterions.
                $guidecrits = array();
                $i = 0;
                foreach ($pacrits as $c) {
                    $gc = new stdClass();
                    $gc->definitionid             = $defid;
                    $gc->sortorder                = ++$i;
                    $gc->shortname                = $c->name;
                    $gc->description              = $c->description;
                    $gc->descriptionformat        = 0;
                    $gc->descriptionmarkers       = $c->description;
                    $gc->descriptionmarkersformat = 0;
                    $gc->maxscore                 = $c->weight / $sumweight * 100;
                    $DB->insert_record('gradingform_guide_criteria', $gc);
                }
            }
        }
        
        // Define table poasassignment_criterions to be dropped.
        $table = new xmldb_table('poasassignment_criterions');
        
        // Conditionally launch drop table for poasassignment_criterions.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }
        
        // Define table poasassignment_rating_values to be dropped.
        $table = new xmldb_table('poasassignment_rating_values');
        
        // Conditionally launch drop table for poasassignment_rating_values.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Poasassignment savepoint reached.
        upgrade_mod_savepoint(true, 2014011300, 'poasassignment');
    }

    if ($result && $oldversion < 2021021800) {
        // Define field descriptionformat to be added to poasassignment_tasks.
        $table = new xmldb_table('poasassignment_tasks');
        $field = new xmldb_field('descriptionformat', XMLDB_TYPE_TEXT, null, null, null, null, null, 'hidden');

        // Conditionally launch add field descriptionformat.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Poasassignment savepoint reached.
        upgrade_mod_savepoint(true, 2021021800, 'poasassignment');
    }

    if ($result && $oldversion < 2021051400) {

        // Define field id to be added to poasassignment_task_values.
        $table = new xmldb_table('poasassignment_task_values');
        $field = new xmldb_field('valueformat', XMLDB_TYPE_INTEGER, '2', null, null, null, null, 'assigneeid');

        // Conditionally launch add field id.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Poasassignment savepoint reached.
        upgrade_mod_savepoint(true, 2021051400, 'poasassignment');
    }

/// And that's all. Please, examine and understand the 3 example blocks above. Also
/// it's interesting to look how other modules are using this script. Remember that
/// the basic idea is to have "blocks" of code (each one being executed only once,
/// when the module version (version.php) is updated.

/// Lines above (this included) MUST BE DELETED once you get the first version of
/// yout module working. Each time you need to modify something in the module (DB
/// related, you'll raise the version and add one upgrade block here.

/// Final return of upgrade result (true/false) to Moodle. Must be
/// always the last line in the script
    return $result;
}
