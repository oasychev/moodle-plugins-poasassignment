<?php
defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once('abstract_page.php');
require_once(dirname(dirname(__FILE__)) . '/model.php');
require_once($CFG->dirroot . '/grade/grading/lib.php');

class grade_page extends abstract_page{
    private $assigneeid;
    private $assignee;
    function __construct() {
        $this->assigneeid = optional_param('assigneeid', 0, PARAM_INT);
    }

    /** Getter of page capability
     * @return capability 
     */
    function get_cap() {
        return 'mod/poasassignment:grade';
    }

    /** Checks module settings that prohibit viewing this page, used in has_ability_to_view
     * @return true if neither setting prohibits
     */
    function has_satisfying_parameters() {
        global $DB;
        if (!$this->assignee = $DB->get_record('poasassignment_assignee', array('id' => $this->assigneeid))) {
            $this->lasterror = 'errornonexistentassignee';
            return false;
        }
        return true;
    }
    
    public function pre_view() {
        global $DB;

        $poasmodel = poasassignment_model::get_instance();
        $cmid = $poasmodel->get_cm()->id;
        $poasassignmentid = $poasmodel->get_poasassignment()->id;
        $this->mform = new grade_form(null, array(
            'id' => $cmid,
            'assigneeid' => $this->assigneeid,
            'poasassignmentid' => $poasassignmentid,
            'grade' => $poasmodel->poasassignment->grade
        ));
        if ($this->mform->is_cancelled()) {
            redirect(new moodle_url('view.php',array('id'=>$cmid,'page'=>'submissions')),null,0);
        }
        else {
            if($data = $this->mform->get_data()) {
                $poasmodel->save_grade($this->assigneeid, $data);

                // Save comment if it wasn't saved before submitting form.
                $commentcontent = optional_param('content', null, PARAM_TEXT);
                if (!empty($commentcontent)) {
                    $options = self::get_comment_settings($poasmodel->get_context(), $data->attemptid);
                    $comment = new comment($options);
                    $comment->add($commentcontent);
                }

                // Trigger submission_graded event
                $attemptscount = $DB->count_records('poasassignment_attempts', array('assigneeid' => $this->assigneeid));
                $attempt = $DB->get_record('poasassignment_attempts', array('assigneeid' => $this->assigneeid, 'attemptnumber' => $attemptscount));
                $params = array(
                    'context'       => context_module::instance($cmid),
                    'objectid'      => $attemptscount,
                    'relateduserid' => $poasmodel->get_user_by_assigneeid($this->assigneeid)->userid,
                    'other'         => array(
                        'isfinal' => $attempt->final
                    )
                );
                $submission_graded_event = \mod_poasassignment\event\submission_graded::create($params);
                $submission_graded_event->trigger();

                redirect(new moodle_url('view.php',array('id'=>$cmid,'page'=>'submissions')),null,0);
            }
        }

    }
    function view() {
        $model = poasassignment_model::get_instance();
        $this->mform->display();
    }
    
    public static function display_in_navbar() {
        return false;
    }

    /**
     * Returns options for comment class.
     * @param context_module $context
     * @param int $itemid
     * @return stdClass
     */
    public static function get_comment_settings(context_module $context, $itemid) {
        $options = new stdClass();
        $options->area = 'poasassignment_comment';
        $options->pluginname = 'poasassignment';
        $options->component = 'mod_poasassignment';
        $options->context = $context;
        $options->itemid = $itemid;
        $options->showcount = true;
        $options->autostart = true;
        $options->notoggle = true;
        return $options;
    }
}
class grade_form extends moodleform {

    function definition(){
        global $DB, $OUTPUT, $USER, $PAGE;
        $mform =& $this->_form;
        $instance = $this->_customdata;
        $assignee = $DB->get_record('poasassignment_assignee',array('id'=>$instance['assigneeid']));
        $poasmodel = poasassignment_model::get_instance();
        $user = $DB->get_record('user',array('id'=>$assignee->userid));
        $attemptscount = $DB->count_records('poasassignment_attempts',array('assigneeid'=>$instance['assigneeid']));
        $attempt = $DB->get_record('poasassignment_attempts',
                                    array('assigneeid' => $instance['assigneeid'],'attemptnumber' => $attemptscount));
        $lateness = format_time(time() - $attempt->attemptdate);
        $poasassignment = $DB->get_record('poasassignment',array('id'=>$instance['poasassignmentid']));
        $attemptsurl = new moodle_url('view.php',array('page' => 'attempts',
                                                       'id' => $instance['id'],
                                                       'assigneeid' => $instance['assigneeid']));
        $userurl = new moodle_url('/user/profile.php',array('id'=>$user->id));
        if ($poasmodel->has_flag(ACTIVATE_INDIVIDUAL_TASKS)) {
            $taskviewurl = new moodle_url('view.php', array('page' => 'taskview', 
                                                            'id' => $instance['id'], 
                                                            'taskid' => $assignee->taskid));
        }
        else {
            $taskviewurl = '';
        }
        $mform->addElement('static', 'picture', $OUTPUT->user_picture($user),
                                                html_writer::link($userurl,fullname($user, true)) . '<br>'.
                                                userdate($attempt->attemptdate) . '<br/>' .
                                                $lateness.' '.get_string('ago','poasassignment').'<br>'.
                                                html_writer::link($attemptsurl,get_string('studentattempts','poasassignment') . '<br>'.
                                                html_writer::link($taskviewurl,get_string('stundetstask','poasassignment'))));
        
        $mform->addElement('header','studentsubmission',get_string('studentsubmission','poasassignment'));
        require_once('attempts.php');
        $mform->addElement('static',null,null,attempts_page::show_attempt($attempt));

        $grademenu = make_grades_menu($instance['grade']);

        // Make grades order from lowest to highest.
        $grademenu = array_reverse($grademenu, true);

        $gradecontroller = get_grading_manager($poasmodel->get_context(), 'mod_poasassignment', 'submissions')->get_active_controller();
        if ($gradecontroller) {
            $gradecontroller->set_grade_range($grademenu);
            $cangrade = has_capability('mod/assign:grade', $poasmodel->get_context());
        }

        // Show comments on previous attempts if have.
        if ($attempt->attemptnumber != 1) {
            $mform->addElement('header', 'prevattemptsheader', get_string('prevattempts', 'poasassignment'));

            $latestattempt = $poasmodel->get_last_attempt($assignee->id);
            $attempts = array_reverse($DB->get_records('poasassignment_attempts', array('assigneeid' => $assignee->id), 'attemptnumber'));
            foreach ($attempts as $curattempt) {
                if ($curattempt != $latestattempt) {
                    $mform->addElement('html', '<h3>' . get_string('attempt', 'poasassignment') . ' ' . $curattempt->attemptnumber
                                       . ($curattempt->draft ? ' (' . get_string('draft', 'poasassignment') . ')' : '') . '</h3>');
                    $mform->addElement('static', null, get_string('submitted', 'poasassignment'), userdate($curattempt->attemptdate));
                    $mform->addElement('static', null, get_string('gradedate', 'poasassignment'), userdate($curattempt->ratingdate));
                    $mform->addElement('static', null, get_string('totalratingis', 'poasassignment'), $curattempt->rating);

                    // If advanced grading.
                    if ($gradecontroller) {
                        $gradefordisplay = $gradecontroller->render_grade($PAGE, $curattempt->id, array(), 'notadvanced', $cangrade);
                        if ($gradefordisplay != 'notadvanced') {
                            $mform->addElement('static', null, get_string('grade', 'poasassignment'), $gradefordisplay);
                        }
                    }

                    $mform->addElement('static', null, get_string('penalty','poasassignment'), $poasmodel->get_penalty($curattempt->id));

                    $commentshtml = attempts_page::show_comments($curattempt->id);
                    $mform->addElement('static', null, get_string('comment', 'poasassignment'),
                        $commentshtml == null ? get_string('nocomments', 'poasassignment') : $commentshtml);
                }
            }
        }

        $mform->addElement('header','gradeeditheader',get_string('gradeeditheader','poasassignment'));
        $mform->setExpanded('gradeeditheader');
        
        // If advanced grading.
        if ($gradecontroller) {
            $instanceid = optional_param('advancedgradinginstanceid', 0, PARAM_INT);
            $gradinginstance = $gradecontroller->get_or_create_instance($instanceid, $USER->id, $attempt->id);
            $mform->addElement('grading', 'advancedgrade', get_string('grade'), array('gradinginstance' => $gradinginstance));
            $mform->addElement('hidden', 'advancedgradinginstanceid', $gradinginstance->get_id());
            $mform->setType('advancedgradinginstanceid', PARAM_INT);
        } else {
            $gradingelement = $mform->addElement('select', 'grade', get_string('grade'), $grademenu);
            if (!empty($attempt->rating)) {
                $mform->setDefault('grade', $attempt->rating);
            }
        }
        
        if($attempt->draft == 0 || has_capability('mod/poasassignment:manageanything', $poasmodel->get_context())) {
            $mform->addElement('checkbox', 'final', get_string('finalgrade','poasassignment'));
        }
        
        $mform->addElement('static','penalty',get_string('penalty','poasassignment'),$poasmodel->get_penalty($attempt->id));

        $options = grade_page::get_comment_settings($poasmodel->get_context(), $attempt->id);
        $comment = new comment($options);
        $mform->addElement('static',
            'generalfeedback',
            get_string('comment', 'poasassignment'),
            $comment->output(true)
        );

        $mform->addElement('filemanager', 'commentfiles_filemanager', get_string('commentfiles','poasassignment'));

        // hidden params
        //$mform->addElement('hidden', 'weightsum', $weightsum);
        //$mform->setType('weightsum', PARAM_FLOAT);
        
        $mform->addElement('hidden', 'id', $instance['id']);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'poasassignmentid', $instance['poasassignmentid']);
        $mform->setType('poasassignmentid', PARAM_INT);
        
        $mform->addElement('hidden', 'assigneeid', $instance['assigneeid']);
        $mform->setType('assigneeid', PARAM_INT);

        $mform->addElement('hidden', 'attemptid', $attempt->id);
        $mform->setType('attemptid', PARAM_INT);
        
        $mform->addElement('hidden', 'page', 'grade');
        $mform->setType('page', PARAM_TEXT);
        
        $this->add_action_buttons(true, get_string('savechanges', 'admin'));
    }
}
