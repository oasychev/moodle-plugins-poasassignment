<?php
global $CFG;
require_once dirname(dirname(__FILE__)).'/taskgiver.php';
require_once($CFG->libdir.'/formslib.php');
class studentschoice extends taskgiver {
    
    /**
     * Available tasks cache
     */
    private $availabletasks = null;
    
    public static function has_settings() {
        return false;
    }
    public static function show_tasks() {
        return true;
    }

    /**
     * Get html to add after task name in table cell
     *
     * @param $taskid poas assignment task id
     * @param $cmid course module id
     * @return mixed html code to add after task name
     */
    function get_task_extra_string($taskid, $cmid) {
        global $USER, $OUTPUT;
        $model = poasassignment_model::get_instance();
        $takeicon = '';
        $hascaptohavetask = has_capability('mod/poasassignment:havetask', poasassignment_model::get_instance()->get_context());
        
        // Set cached value.
        if ($this->availabletasks === null) {
            $this->availabletasks = $model->get_available_tasks($USER->id);
        }
        
        // Check if the task if available to choosing for the given user.
        $available = false;
        foreach ($this->availabletasks as $task) {
            if($task->id == $taskid) {
                $available = true;
                break;
            }
        }
        
        if ($hascaptohavetask && !$model->check_dates() && $available) {
            // Require mod/poasassignment:havetask to show 'take task' link.
            $takeurl = new moodle_url('warning.php?id='.$cmid.'&action=taketask&taskid='.$taskid.'&userid='.$USER->id);
            $takeicon = '<a href="'.$takeurl.'">'.'<img src="'.$OUTPUT->image_url('taketask','poasassignment').
                '" class="iconsmall" alt="'.get_string('view').'" title="'.get_string('taketask','poasassignment').'" /></a>';
        }
        return $takeicon;
    }
}
?>
