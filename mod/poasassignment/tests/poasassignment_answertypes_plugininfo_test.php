<?php
// This file is part of Poas Assignment plugin - https://bitbucket.org/oasychev/moodle-plugins/
//
// Poas Assignment plugin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Poas Assignment plugin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Poas Assignment plugin.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines unit-tests for "Answer Types" subplugins PluginInfo classes
 *
 * @copyright &copy; 2011 Oleg Sychev
 * @author Alexander Lyashenko, Volgograd State Technical University
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_poasassignment
 */
use mod_poasassignment\plugininfo\poasassignmentanswertypes as poasassignmentanswertypes;

global $CFG;
require_once($CFG->dirroot.'/mod/poasassignment/classes/plugininfo/poasassignmentanswertypes.php');

/**
 * Tests deleting "Answer Types" subplugins
 */
class poasassignment_answertypes_plugininfo_test extends \advanced_testcase {
    public function test_clearing_answerfile(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignmentanswertypes();
        $testclass->name = "answer_file";
        $pluginid = $DB->get_field('poasassignment_answers', 'id', array('name' => 'answer_file'));

        $testfiles = array();
        $testsubmissions = array();
        $testansstrings = array();
        $testrecordscount = 4;

        for ($index = 0; $index < $testrecordscount; $index++){
            $submissionrecord = new stdClass();
            $submissionrecord->answerid = $pluginid;
            $submissionrecord->value = 666666666 - $index;
            $submissionrecord->attemptid = 0;
	        array_push($testsubmissions,$submissionrecord);

            $filerecord = new stdClass();
            $filerecord->contenthash = md5($index);
            $filerecord->pathnamehash = md5($index);
            $filerecord->contextid = 0;
	        $filerecord->component = $index % 2 == 0 ? 'user' : 'mod_poasassignment';
	        $filerecord->filearea = $index % 2 == 0 ? 'draft' : 'submissionfiles';
	        $filerecord->itemid = $submissionrecord->value;
	        $filerecord->filepath = '/';
	        $filerecord->filename = $index % 2 == 0 ? '.' : 'Test1.txt';
	        $filerecord->userid = 0;
	        $filerecord->filesize = 0;
	        $filerecord->mimetype = $index % 2 == 0 ? '' : 'application/zip';
	        $filerecord->status = 0;
	        $filerecord->source = $index % 2 == 0 ? '' : "Test$index.txt";
	        $filerecord->author = $index % 2 == 0 ? '' : 'Tester';;
	        $filerecord->license = $index % 2 == 0 ? '' : 'allrightsended';
	        $filerecord->timecreated = 0;
	        $filerecord->timemodified = 0;
	        $filerecord->sortorder = 0;
        	$filerecord->referencefileid = 0;
        	array_push($testfiles, $filerecord);

        	$ansstringrecord = new stdClass();
            $ansstringrecord->value = $index % 2;
            $ansstringrecord->name = 'name';
            $ansstringrecord->answerid = $pluginid;
            $ansstringrecord->poasassignmentid = $index;

        }

        $DB->insert_records('poasassignment_submissions', $testsubmissions);
        $DB->insert_records('files', $testfiles);
        $DB->insert_records('poasassignment_ans_strings', $testansstrings);
        $records = $DB->get_fieldset_select(
            "poasassignment_submissions",
            "value",
            "answerid = $pluginid");

        $testclass->uninstall_cleanup();

        $this->assertCount(0, $DB->get_records('poasassignment_submissions', array('answerid' => $pluginid)));
        $this->assertCount(0, $DB->get_recordset_select('files',
            "itemid IN (" . implode(',',$records
                ) . ") OR component = \"mod_poasassignment\""));
        $this->assertCount(0, $DB->get_records('poasassignment_ans_stngs', array('answerid' => $pluginid)));
        $this->assertCount(0, $DB->get_records('poasassignment_answers', array('id' => $pluginid)));
    }

    public function test_clearing_answertext(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignmentanswertypes();
        $testclass->name = "answer_text";
        $pluginid = $DB->get_field('poasassignment_answers', 'id', array('name' => 'answer_text'));

        $testsubmissions = array();
        $testansstrings = array();
        $testrecordscount = 4;

        for ($index = 0; $index < $testrecordscount; $index++){
            $submissionrecord = new stdClass();
            $submissionrecord->answerid = $pluginid;
            $submissionrecord->value = 666666666 - $index;
            $submissionrecord->attemptid = 0;
            array_push($testsubmissions,$submissionrecord);

            $ansstringrecord = new stdClass();
            $ansstringrecord->value = $index % 2;
            $ansstringrecord->name = 'name';
            $ansstringrecord->answerid = $pluginid;
            $ansstringrecord->poasassignmentid = $index;

        }

        $DB->insert_records('poasassignment_submissions', $testsubmissions);
        $DB->insert_records('poasassignment_ans_strings', $testansstrings);
        $records = $DB->get_fieldset_select(
            "poasassignment_submissions",
            "value",
            "answerid = $pluginid");

        $testclass->uninstall_cleanup();

        $this->assertCount(0, $DB->get_records('poasassignment_submissions', array('answerid' => $pluginid)));
        $this->assertCount(0, $DB->get_records('poasassignment_ans_stngs', array('answerid' => $pluginid)));
        $this->assertCount(0, $DB->get_records('poasassignment_answers', array('id' => $pluginid)));
    }
}
