<?php

// Testing generator of poasassignment

defined('MOODLE_INTERNAL') || die();

/**
 * poasassignment module data generator class
 *
 * @package mod_assign
 * @category test
 */
class mod_poasassignment_generator extends testing_module_generator {
    public function create_instance($record = null, array $options = null) {
        $record = (object)(array)$record;

        // Need integer id of task selecting
        if (isset($record->taskgiverid)) {
            $record->taskgiverid = (int) $record->taskgiverid;
        }

        // Set text of introeditor as description
        $text = isset($record->description) ? $record->description : 'Test poasassignment';

        $availabledate = isset($record->beforeavailabledate) ? time()+3600 : time(); // 1 hour

        if (isset($record->afterchoicedate)) {
            $availabledate = time();
            $choicedate = time()+60; // 1 min
        } else {
            $choicedate = time()+2*24*3600;
        }

        if (isset($record->afterduedate)) {
            $availabledate = time();
            $choicedate = time()+90; // 1.5 min
            $duedate = time()+120; // 2 min
        } else {
            $duedate = time()+7*24*3600;
        }

        $defaultsettings = array(
            'name' => 'Test poasassignment',
            'introeditor' => array(
                'text' => "<p>$text</p>",
                'format' => '1',
                'itemid' => (int) str_replace('.', '', microtime(true)) // required id
            ),

            // Files
            'poasassignmentfiles' => (int) str_replace('.', '', microtime(true)), // required

            // Date
            'availabledate' => $availabledate, // or 0
            'choicedate' => $choicedate, // or 0
            'deadline' => $duedate, // or 0

            // 'preventlatechoice' => '1',
            // 'randomtasksafterchoicedate' => '1',
            // 'preventlate' => '1'

            // Individual tasks
            // 'activateindividualtasks' => '1',
            // 'taskgiverid' => 3,  // random task selecting available 1, 2 or 3
            // 'uniqueness' => '1',
            // 'secondchoice' => '1',
            // 'preventlate' => '1',
            // 'teacherapproval' => '1',

            // Answers
            // 'severalattempts' => '1',
            // 'newattemptbeforegrade' => '1',
            // 'finalattempts' => '1',
            // 'penalty' => 0,

            // Answer file
            // 'answerfile' => '1',
            // 'fileamount' => '-1',
            // 'maxfilesize' => '0',
            // 'fileextensions' => '',

            // Answer text
            // 'answertext' => '1',

            // Grade
            'grade' => 100,
            'advancedgradingmethod_submissions' => '',
            'gradecat' => 1,
            'gradepass' => '',
            // Common module settings
            'visible' => '1',
            'cmidnumber' => '',
            'groupmode' => '0',
            'groupingid' => '0',

            // course => 2,
            // 'coursemodule' => 0,

            'instance' => 0, // if create then 0
            'update' => 0,  // if create then 0
            'section' => 0,
            'add' => 'poasassignment',
            'return' => 0,  // if create then 0
            'sr' => 0,
            'module' => 23,
            'modulename' => 'poasassignment',
        );

        foreach ($defaultsettings as $name => $value) {
            if (!isset($record->{$name})) {
                $record->{$name} = $value;
            }
        }

        return parent::create_instance($record, (array)$options);
    }
}
