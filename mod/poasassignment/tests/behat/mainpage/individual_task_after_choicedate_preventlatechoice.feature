@mod @mod_poasassignment @core_role @core_access
Feature: In main view of poasassignment, student can not take individual task after choice date and preventlatechoice mode on
  In order to take individual task in main view of poasassignment
  As a student
  I can not take individual task

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name             | description    | afterchoicedate | preventlatechoice |activateindividualtasks | taskgiverid | uniqueness |answertext |
      | poasassignment | C1     | pas1     | Poasassignment 1 | Test task intro| 1               |1                  |1                       | 3           |0           | 1         |

  # Overview
  #
  # Found bugs:
  # 1) Cannot create multiple scenarios with answer text. The first scenario execute successfully, then in the next scenarios
  # do not have to insert the text field response
  # 2) Cannot create multiple instance of poasassignment module
  #
  # Test success:
  # 1) Scenario: Student can not take individual task after choice date and preventlatechoice mode on
  #
  # Test fail:
  # -

  @javascript
  Scenario: Student can not take individual task after choice date and preventlatechoice mode on
    Given I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Tasks" "link" in the "Navigation" "block"
    And I press "Create task"
    And I set the field "name" to "task1"
    And I press "Save changes"
    And I press "Create task"
    And I set the field "name" to "task2"
    And I press "Save changes"
    And I log out
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "You had to choose task"
    And I should not see "You have no task Click here to take task"
