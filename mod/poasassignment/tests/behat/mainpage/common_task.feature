@mod @mod_poasassignment @core_role @core_access
Feature: In main view of poasassignment, user can manipulate with students submissions and attempts for common task to depending on a role
  In order to see possible actions with common task on view page of poasassignment according to a role
  As a manager or course creator or teacher or student
  Manager, teacher and  course creator can see attempts of students and can add and give task to students, student can see only own attempts and common task

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name             | description    | answertext |availabledate | choicedate | deadline | severalattempts | penalty | finalattempts | fileamount | maxfilesize | fileextensions |
      | poasassignment | C1     | pas1     | Poasassignment 1 | Test task intro| 1          |0             | 0          | 0        | 1               | 0       | 1             | -1         | 0           |                |

  # Overview
  #
  # Found bugs:
  # 1) Cannot create multiple scenarios with answer text. The first scenario execute successfully, then in the next scenarios
  # do not have to insert the text field response
  # 2) Cannot create multiple instance of poasassignment module
  #
  # Test success:
  # 2) Scenario: Manager can see attempts of students for common task
  #
  # Test fail:
  # 1) Scenario: Creator of a course can see attempts of students for common task
  # 3) Scenario: Student can add submission for common task
  # 4) Scenario: Student add a submission as a final attempt for common task
  # 5) Scenario: Student add a submission as a draft attempt for common task
  # 6) Scenario: Student add a submission for common task, then edit submission and teacher can see student's attempt
  # 7) Scenario: Teacher evaluate and comment student's attempt for common task and then student see a teacher's grade and comment it
  # 8) Scenario: Teacher comment a student's attempt for common task and finalize grade and then student see a teacher's grade

  @javascript
  Scenario: Creator of a course can see attempts of students for common task
    When I log in as "creator"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "0 of 2 need grade"
    And I should not see "You have no task Click here to take task"
    And I follow "Poasassignment 1"
    And I should see "Poasassignment 1 : View"
    And I should see "Task description"
    And I should see "Test task intro"

  @javascript
  Scenario: Manager can see attempts of students for common task
    When I log in as "manager"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "0 of 2 need grade"
    And I should not see "You have no task Click here to take task"
    And I follow "Poasassignment 1"
    And I should see "Poasassignment 1 : View"
    And I should see "Task description"
    And I should see "Test task intro"

  @javascript
    Scenario: Student can add submission for common task and teacher can see students attempts
    Given I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I should not see "You have no task Click here to take task"
    And I should see "Poasassignment 1 : View"
    And I should see "Task description"
    And I should see "Test task intro"
    And I should see "0 of 2 need grade"
    And I log out
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "Poasassignment 1 : View"
    And I should see "Task description"
    And I should see "Test task intro"
    And I should not see " need grade"
    And I should not see "You have no task Click here to take task"
    And I press "Add submission"
    And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I should see "Last attempt"
    And I should see "1" in the "Attempt number" "table_row"
    And I should see "No" in the "Draft" "table_row"
    And I should see "No" in the "Attempt is final" "table_row"
    And I should see "answer1" in the "poasassignment-table" "table"
    And I should not see "You have no task Click here to take task"
    And I should not see " need grade"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I should see "1 of 2 need grade"

  @javascript
  Scenario: Student add a submission as a final attempt for common task and teacher can see students attempts
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I press "Add submission"
    And I set the field "final" to "1"
    # And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I should see "Last attempt"
    And I should see "1" in the "Attempt number" "table_row"
    And I should see "No" in the "Draft" "table_row"
    And I should see "Yes" in the "Attempt is final" "table_row"
    # And I should see "answer1" in the "poasassignment-table" "table"
    Then "Edit submission" "button" should not exist
    And I should not see " need grade"
    And I should not see "You have no task Click here to take task"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I should see "1 of 2 need grade"

  @javascript
  Scenario: Student add a submission as a draft attempt for common task and teacher can see student attempts
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I press "Add submission"
    And I set the field "draft" to "1"
   # And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I should see "Last attempt"
    And I should see "1" in the "Attempt number" "table_row"
    And I should see "Yes" in the "Draft" "table_row"
    And I should see "No" in the "Attempt is final" "table_row"
   # And I should see "answer1" in the "poasassignment-table" "table"
    Then "Edit submission" "button" should exist
    And I should not see " need grade"
    And I should not see "You have no task Click here to take task"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I should see "0 of 2 need grade"

  @javascript
  Scenario: Student add a submission for common task, then edit submission and teacher can see students attempts
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I press "Add submission"
    # And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I should see "Last attempt"
    And I should see "1" in the "Attempt number" "table_row"
    And I should see "No" in the "Draft" "table_row"
    And I should see "No" in the "Attempt is final" "table_row"
    # And I should see "answer1" in the "poasassignment-table" "table"
    And I press "Edit submission"
    # And I set the field "text_editor" to "answer2"
    And I press "Send submission"
    Then I should see "Last attempt"
    And I should see "2" in the "Attempt number" "table_row"
    And I should see "No" in the "Draft" "table_row"
    And I should see "No" in the "Attempt is final" "table_row"
    # And I should see "answer2" in the "poasassignment-table" "table"
    And I should not see "You have no task Click here to take task"
    And I should not see " need grade"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I should see "1 of 2 need grade"

  @javascript
  Scenario: Teacher evaluate and comment student attempt for common task and then student see a teacher grade and comment it
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I press "Add submission"
    # And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I follow "1 of 2 need grade"
    And I click on "Add grade" "link" in the "Student S1" "table_row"
    And I set the following fields to these values:
      | grade | 50 / 100 |
      | content | I'm the teacher feedback |
    And I press "Save changes"
    And I log out
    And I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "Last graded attempt"
    And I should see "Total grade is: 50.00"
    And "Edit submission" "button" should exist
    And I click on "Comments" "link"
    And I should see "I'm the teacher feedback" in the ".comment-list" "css_element"
    And I set the field "content" to "I'm the student feedback"
    And I click on "Save comment" "link"
    # Wait for the animation to finish.
    And I wait "2" seconds
    And I should see "I'm the student feedback" in the ".comment-list" "css_element"

  @javascript
  Scenario: Teacher comment a student attempt for common task and finalize grade and then student see a teacher grade
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I press "Add submission"
    # And I set the field "text_editor" to "answer1"
    And I press "Send submission"
    And I log out
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I follow "1 of 2 need grade"
    And I click on "Add grade" "link" in the "Student S1" "table_row"
    And I set the following fields to these values:
      | grade | 50 / 100 |
      | content | I'm the teacher feedback |
      | final   | 1                        |
    And I press "Save changes"
    And I log out
    And I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    Then I should see "Last graded attempt"
    And I should see "Total grade is: 50.00"
    And "Edit submission" "button" should not exist
    And I click on "Comments" "link"
    And I should see "I'm the teacher feedback" in the ".comment-list" "css_element"
