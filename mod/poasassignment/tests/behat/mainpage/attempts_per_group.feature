@mod @mod_poasassignment
Feature: Attempts per group
  In order to see attempts of users like the submissions one
  As a teacher
  I need to be able to see students' attempts per group

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student1  | S1         | student1@example.com  |
      | student2        | Student2  | S2         | student2@example.com  |
      | student3        | Student3  | S3         | student3@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
      | student3      | C1     | student        |
    And the following "groups" exist:
      | name    | course | idnumber |
      | Group 1 | C1     | GROUP1   |
      | Group 2 | C1     | GROUP2   |
    And the following "activities" exist:
      | activity       | course | idnumber | name             | availabledate | choicedate | deadline | severalattempts | penalty | finalattempts | fileamount | maxfilesize | fileextensions | answertext | activateindividualtasks | taskgiverid | uniqueness | groupmode |
      | poasassignment | C1     | pas1     | Poasassignment 1 | 0             | 0          | 0        | 1               | 0       | 1             | -1         | 0           |                | 1          | 1                       | 3           | 0          | 1         |

  @javascript
  Scenario: There are two groups, first one consist of two students and second one consist of one student
    Given the following "group members" exist:
      | user     | group  |
      | student1 | GROUP1 |
      | student2 | GROUP1 |
      | student2 | GROUP2 |
    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I follow "Tasks"
    And I press "Create task"
    And I set the field "name" to "PoasTask1"
    And I press "Save changes"
    And I log out

    And I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Click here to take task" "link"
    And I press "Add submission"
    And I set the field "text_editor" to "student1 answer1"
    And I press "Send submission"
    And I log out

    And I log in as "student2"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Click here to take task" "link"
    And I press "Add submission"
    And I set the field "text_editor" to "student2 answer2"
    And I press "Send submission"
    And I log out

    And I log in as "student3"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Click here to take task" "link"
    And I press "Add submission"
    And I set the field "text_editor" to "student3 answer3"
    And I press "Send submission"
    And I log out

    And I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I follow "Attempts"

    When I set the field "group" to "Group 1"
    Then the "assigneeid" select box should contain "Student1 S1"
    And the "assigneeid" select box should contain "Student2 S2"
    And "poasassignment-table" "table" should not exist

    When I set the field "assigneeid" to "Student1 S1"
    And I press "go"
    Then "poasassignment-table" "table" should exist

    When I set the field "assigneeid" to "Student2 S2"
    And I press "go"
    Then "poasassignment-table" "table" should exist

    When I set the field "group" to "Group 2"
    Then the "assigneeid" select box should contain "Student2 S2"
    And "poasassignment-table" "table" should not exist

    When I set the field "assigneeid" to "Student2 S2"
    And I press "go"
    Then "poasassignment-table" "table" should exist

    When I set the field "group" to "All participants"
    Then the "assigneeid" select box should contain "Student1 S1"
    And the "assigneeid" select box should contain "Student2 S2"
    And the "assigneeid" select box should contain "Student3 S3"
    And "poasassignment-table" "table" should not exist

    When I set the field "assigneeid" to "Student3 S3"
    And I press "go"
    Then "poasassignment-table" "table" should exist

