@mod @mod_poasassignment @core_role @core_access
Feature: In main view of poasassignment, student can see uploaded files for own attempts
  In order to see can see uploaded files
  As a student
  I need see uploaded files from my file system to be assessed

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name             | description    | activateindividualtasks | taskgiverid | uniqueness |availabledate | choicedate | deadline | answerfile | fileamount | maxfilesize | fileextensions |
      | poasassignment | C1     | pas1     | Poasassignment 1 | Test task intro| 1                       | 3           |0           |0             | 0          | 0        | 1          | -1         | 0           |                |

  # Overview
  #
  # Found bugs:
  # 1) Cannot create multiple scenarios with answer text. The first scenario execute successfully, then in the next scenarios
  # do not have to insert the text field response
  # 2) Cannot create multiple instance of poasassignment module
  #
  # Test success:
  # 1) Scenario: Student can see uploaded files for own attempts
  #
  # Test fail:
  # -

  @javascript
  Scenario: Student can see uploaded files for own attempts
    Given I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Tasks" "link" in the "Navigation" "block"
    And I press "Create task"
    And I set the field "name" to "task1"
    And I press "Save changes"
    And I press "Create task"
    And I set the field "name" to "task2"
    And I press "Save changes"
    And I log out
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I follow "Click here to take task"
    And I press "Add submission"
    And I upload "lib/tests/fixtures/empty.txt" file to "Load files" filemanager
    And I press "Send submission"
    Then I should see "Last attempt"
    And I should see "1" in the "Attempt number" "table_row"
    And I should see "No" in the "Draft" "table_row"
    And I should see "No" in the "Attempt is final" "table_row"
    And "empty.txt" "link" should exist
