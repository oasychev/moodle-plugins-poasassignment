@mod @mod_poasassignment @core_role @core_access
Feature: Testing add answer as files

	Background:
    Given the following "courses" exist:
		| fullname    | shortname | category | groupmode |
		| Test Course | TC        | 0        | 1         |
	And the following "users" exist:
		| username        | firstname | lastname   | email                 |
		| teacher         | Teacher   | T          | teacher@example.com   |
		| student1        | Student   | S1         | student1@example.com  |
	And the following "course enrolments" exist:
		| user          | course | role           |
		| teacher       | TC     | editingteacher |
		| student1      | TC     | student        |
	And the following "activities" exist:
		| activity       | course | idnumber | name                | intro                     | activateindividualtasks | taskgiverid | uniqueness | answerfile | answertext | maxfilesize | fileamount    | fileextensions    |
		| poasassignment | TC     | pas1     | Poasassignment Test | Test Poasassignment intro | 1                       | 3           | 0          | 1          | 0          | 0           | -1            |                   |
	And I log in as "teacher"
		And I am on site homepage
		And I follow "Test Course"
		And I follow "Poasassignment Test"
		And I click on "Tasks" "link" in the "Navigation" "block"
		And I press "Create task"
		And I set the field "name" to "newtask"
		And I press "Save changes"
		And I log out
		# We can take task
		And I log in as "student1"
		And I am on site homepage
		And I follow "Test Course"
		And I follow "Poasassignment Test"
		And "Click here to take task" "link" should exist
		And I follow "Click here to take task"
		And I should see "Your task is newtask"
		And I press "Add submission"
		And I click on "Expand all" "link"
		# Fails Here
		Then I should not see "maximum attachments"
		And I should not see "Maximum size for new files"
		And I should not see "Files types"
		# And wtf? I switch-off answertext!!!
		# And I set the field "text_editor" to "someanswer"
		And "text_editor" "field" should not exist

	@javascript
	Scenario: No files added
		Given I press "Send submission"
		Then I should see "No files added"

	@javascript
	Scenario: Add one small text file
		Given I upload "lib/tests/fixtures/empty.txt" file to "Load files" filemanager
		Then I should see "1" elements in "Load files" filemanager
		When I press "Send submission"
		Then I should see "Last attempt"
		And "empty.txt" "link" should exists

	@javascript
	Scenario: Add two files with different extensions and different size
		Given I upload "lib/tests/fixtures/empty.txt" file to "Load files" filemanager
		And I should see "1" elements in "Load files" filemanager
		And I upload "lib/tests/fixtures/timezonewindows.xml" file to "Load files" filemanager
		And I should see "2" elements in "Load files" filemanager
		When I press "Send submission"
		Then I should see "Last attempt"
		And "empty.txt" "link" should exists
		And "timezonewindows.xml" "link" should exists
