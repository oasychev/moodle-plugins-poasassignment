@mod @mod_poasassignment @core_role @core_access
Feature: Testing students the opportunity to answer a text

	Background:
    Given the following "courses" exist:
		| fullname    | shortname | category | groupmode |
		| Test Course | TC        | 0        | 1         |
	And the following "users" exist:
		| username        | firstname | lastname   | email                 |
		| teacher         | Teacher   | T          | teacher@example.com   |
		| student1        | Student   | S1         | student1@example.com  |
	And the following "course enrolments" exist:
		| user          | course | role           |
		| teacher       | TC     | editingteacher |
		| student1      | TC     | student        |
	And the following "activities" exist:
		| activity       | course | idnumber | name                | intro                     | activateindividualtasks | taskgiverid | uniqueness | answertext | answerfile   |
		| poasassignment | TC     | pas1     | Poasassignment Test | Test Poasassignment intro | 1                       | 3           | 0          | 1          | 0            |
	And I log in as "teacher"
		And I am on site homepage
		And I follow "Test Course"
		And I follow "Poasassignment Test"
		And I click on "Tasks" "link" in the "Navigation" "block"
		And I press "Create task"
		And I set the field "name" to "newtask"
		And I press "Save changes"
		And I log out
		# We can take task
		And I log in as "student1"
		And I am on site homepage
		And I follow "Test Course"
		And I follow "Poasassignment Test"
		And "Click here to take task" "link" should exist
		And I follow "Click here to take task"
		And I should see "Your task is newtask"
		And I press "Add submission"


	@javascript
	Scenario: Testing empty and fully text answer field
		Given I should see "Online text as an answer"
		And I should see "Enter your submission here"
		When I press "Send submission"
		Then I should see "Type your answer here"
		And ".filemanager" "css_element" should not exist
		When I set the field "text_editor" to "someanswer"
		And I press "Send submission"
		Then I should see "Last attempt"
		And I should see "someanswer"
