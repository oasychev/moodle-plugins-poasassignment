<?php

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once("$CFG->dirroot/mod/poasassignment/model.php");
require_once("$CFG->dirroot/mod/poasassignment/tests/generator/lib.php");
require_once("$CFG->dirroot/group/lib.php");

class mod_poasassignment_teachers_notification_testcase extends advanced_testcase {
    const ASSERT_NONE = 0;
    const ASSERT_FIRST = 1;
    const ASSERT_ALL = 2;

    const GROUP_DIST_NONE = 0;
    const GROUP_DIST_USER_IN_GROUP_TEACHERS_ARE_NOT = 1;
    const GROUP_DIST_USER_AND_TEACHER1_IN_GROUP1_TEACHER2_IS_IN_GROUP_2 = 2;

    protected $donotnotifyteachersoptions = [
        'notifyteachers' => 0,
    ];

    protected $notifyteachersoptions = [
        'notifyteachers' => POASASSIGNMENT_NOTIFY_TEACHERS,
    ];

    protected $notifyteachersonlyingroupoptions = [
        'notifyteachers' => POASASSIGNMENT_NOTIFY_TEACHERS_ONLY_IN_GROUP + POASASSIGNMENT_NOTIFY_TEACHERS,
    ];

    /**
     * @param array $actualteachers
     * @param stdClass $teacher1
     * @param stdClass $teacher2
     */
    public function assert_nobody_from_teachers_notified($actualteachers, $teacher1, $teacher2) {
        $this->assertCount(0, $actualteachers);
    }

    /**
     * @param array $actualteachers
     * @param stdClass $teacher1
     * @param stdClass $teacher2
     */
    public function assert_first_teacher_notified($actualteachers, $teacher1, $teacher2) {
        $this->assertCount(1, $actualteachers);
        $this->assertEquals($teacher1->id, $actualteachers[0]->id);
    }

    /**
     * @param array $actualteachers
     * @param stdClass $teacher1
     * @param stdClass $teacher2
     */
    public function assert_all_teachers_notified($actualteachers, $teacher1, $teacher2) {
        $this->assertCount(2, $actualteachers);
        $this->assertEquals($teacher1->id, $actualteachers[0]->id);
        $this->assertEquals($teacher2->id, $actualteachers[1]->id);
    }

    /**
     * @param array $options
     * @param $whattoassert
     * @param $groupdist
     * @dataProvider notification_provider
     */
    public function test_notification($options, $whattoassert, $groupdist) {
        $this->resetAfterTest();

        $gen = static::getDataGenerator();
        global $COURSE;
        $COURSE = $gen->create_course();

        $user = $gen->create_user();
        $gen->enrol_user($user->id, $COURSE->id, 'student');
        static::setUser($user);

        $teacher1 = $gen->create_user();
        $gen->enrol_user($teacher1->id, $COURSE->id, 'teacher');
        $teacher2 = $gen->create_user();
        $gen->enrol_user($teacher2->id, $COURSE->id, 'teacher');

        if ($groupdist === self::GROUP_DIST_NONE) {
            'Do nothing';
        } else if ($groupdist === self::GROUP_DIST_USER_IN_GROUP_TEACHERS_ARE_NOT) {
            $groupid = groups_create_group((object)[
                'courseid' => $COURSE->id,
                'name' => 'group1',
            ]);
            groups_add_member($groupid, $user);
        } else if ($groupdist === self::GROUP_DIST_USER_AND_TEACHER1_IN_GROUP1_TEACHER2_IS_IN_GROUP_2) {
            $group1id = groups_create_group((object)[
                'courseid' => $COURSE->id,
                'name' => 'group1',
            ]);
            groups_add_member($group1id, $user);
            groups_add_member($group1id, $teacher1);

            $group2id = groups_create_group((object)[
                'courseid' => $COURSE->id,
                'name' => 'group2',
            ]);
            groups_add_member($group2id, $teacher2);
        }

        $modulegenerator = new mod_poasassignment_generator($gen);
        $poasassignment = $modulegenerator->create_instance(
            (object)array_merge(['course' => $COURSE], $options)
        );

        $model = poasassignment_model::get_instance();
        $model->cash_instance($poasassignment->id);

        $teachers = array_values($model->get_teachers($user));

        if ($whattoassert === self::ASSERT_NONE) {
            $this->assert_nobody_from_teachers_notified($teachers, $teacher1, $teacher2);
        } else if ($whattoassert === self::ASSERT_FIRST) {
            $this->assert_first_teacher_notified($teachers, $teacher1, $teacher2);
        } else if ($whattoassert === self::ASSERT_ALL) {
            $this->assert_all_teachers_notified($teachers, $teacher1, $teacher2);
        }
    }

    public function notification_provider() {
        return [
            'User and teachers doesn\'t belong to any group.
             POASASSIGNMENT_DO_NOT_NOTIFY_TEACHERS setting is set.
             Nobody should be notified.' => [
                $this->donotnotifyteachersoptions,
                self::ASSERT_NONE,
                self::GROUP_DIST_NONE,
            ],
            'User and teachers doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS_ONLY_IN_GROUP setting is set.
             All teachers should be notified.' => [
                $this->notifyteachersonlyingroupoptions,
                self::ASSERT_ALL,
                self::GROUP_DIST_NONE,
            ],
            'User and teachers doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS setting is set.
             All teachers should be notified.' => [
                $this->notifyteachersoptions,
                self::ASSERT_ALL,
                self::GROUP_DIST_NONE,
            ],
            'User belong to group1, teachers doesn\'t belong to any group.
             POASASSIGNMENT_DO_NOT_NOTIFY_TEACHERS setting is set.
             Nobody should be notified.' => [
                $this->donotnotifyteachersoptions,
                self::ASSERT_NONE,
                self::GROUP_DIST_USER_IN_GROUP_TEACHERS_ARE_NOT,
            ],
            'User belong to group1, teachers doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS_ONLY_IN_GROUP setting is set.
             All teachers should be notified.' => [
                $this->notifyteachersonlyingroupoptions,
                self::ASSERT_ALL,
                self::GROUP_DIST_USER_IN_GROUP_TEACHERS_ARE_NOT,
            ],
            'User belong to group1, teachers doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS setting is set.
             All teachers should be notified.' => [
                $this->notifyteachersoptions,
                self::ASSERT_ALL,
                self::GROUP_DIST_USER_IN_GROUP_TEACHERS_ARE_NOT,
            ],
            'User and teacher1 belong to group1, teacher2 doesn\'t belong to any group.
             POASASSIGNMENT_DO_NOT_NOTIFY_TEACHERS setting is set.
             Nobody should be notified.' => [
                $this->donotnotifyteachersoptions,
                self::ASSERT_NONE,
                self::GROUP_DIST_USER_AND_TEACHER1_IN_GROUP1_TEACHER2_IS_IN_GROUP_2,
            ],
            'User and teacher1 belong to group1, teacher2 doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS_ONLY_IN_GROUP setting is set.
             Teacher1 should be notified and teacher2 shouldn\'t.' => [
                $this->notifyteachersonlyingroupoptions,
                self::ASSERT_FIRST,
                self::GROUP_DIST_USER_AND_TEACHER1_IN_GROUP1_TEACHER2_IS_IN_GROUP_2,
            ],
            'User and teacher1 belong to group1, teacher2 doesn\'t belong to any group.
             POASASSIGNMENT_NOTIFY_TEACHERS setting is set.
             All teachers should be notified.' => [
                $this->notifyteachersoptions,
                self::ASSERT_ALL,
                self::GROUP_DIST_USER_AND_TEACHER1_IN_GROUP1_TEACHER2_IS_IN_GROUP_2,
            ],
        ];
    }
}