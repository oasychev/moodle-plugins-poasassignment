<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * POAS Assignment auditor connection subplugin info class.
 *
 * @package    mod_poasassignment
 * @author     Nikita Kalinin <nixorv@gmail.com>
 * @copyright  2014 Oleg Sychev (Volgograd State Technical University) <oasychev@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_poasassignment\plugininfo;

use core\plugininfo\base, core_plugin_manager, moodle_url;

defined('MOODLE_INTERNAL') || die();

class poasassignmentadditional extends base {
    public function is_uninstall_allowed() {
        return false;
    }

    public function uninstall_cleanup(){
        global $DB;

        $DB->delete_records('auditor_sync_tasks');

        parent::uninstall_cleanup();
    }
}
